package com.kshrd.homework004;

public class EmailModel {

    private String email;

    public EmailModel(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "EmailModel{" +
                "email='" + email + '\'' +
                '}';
    }

}
