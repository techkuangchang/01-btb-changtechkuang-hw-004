package com.kshrd.homework004;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(Configuration.ORIENTATION_PORTRAIT == getResources().getConfiguration().orientation) {
            //Only Portrait Text and Button
            MyFragment myFragment = new MyFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_layout, myFragment);
            fragmentTransaction.commit();
        }
        else if(Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation) {

            //Portrait TextView and Button
            MyFragment myFragment = new MyFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.layout_btn_left, myFragment);

//            //Landscape List View
            MyListFragment myListFragment = new MyListFragment();
            fragmentTransaction.replace(R.id.layout_list_right, myListFragment);
            fragmentTransaction.commit();
        }
    }
}