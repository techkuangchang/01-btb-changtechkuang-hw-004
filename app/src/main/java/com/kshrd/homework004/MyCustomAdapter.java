package com.kshrd.homework004;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MyCustomAdapter extends RecyclerView.Adapter<MyCustomAdapter.ViewHolder> {

    private String message="TAG";

    private Context context;
    private List<EmailModel> emailModelList;
    private TextView mTxtList;

    public MyCustomAdapter(Context context, List<EmailModel> emailModelList){
        this.context = context;
        this.emailModelList = emailModelList;
    }

    //Initialization data
    @NonNull
    @Override
    public MyCustomAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_list_fragment, parent, false);
        mTxtList = view.findViewById(R.id.txt_list_email_fragment);

        //Change TextColor Back ListView Portrait to Color White When Landscape
        if(Configuration.ORIENTATION_LANDSCAPE == view.getResources().getConfiguration().orientation){
            mTxtList.setTextColor(Color.WHITE);
        }

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyCustomAdapter.ViewHolder holder, int position) {
        EmailModel emailModel = emailModelList.get(position);
        holder.getView().setText(emailModel.getEmail());
        Log.d(message, String.valueOf(emailModelList.get(position)));
    }

    @Override
    public int getItemCount() {
        Log.d(message, String.valueOf(emailModelList.size()));
        return emailModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtViewList;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            txtViewList = itemView.findViewById(R.id.txt_list_email_fragment);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(itemView.getContext(), txtViewList.getText().toString(), Toast.LENGTH_SHORT).show();
                }
            });

        }

        public TextView getView(){
            return txtViewList;
        }
    }
}
