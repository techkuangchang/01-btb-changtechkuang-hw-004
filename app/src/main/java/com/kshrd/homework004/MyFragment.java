package com.kshrd.homework004;

import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class MyFragment extends Fragment {

    TextView mTxtFragment;
    Button mBtnFragment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.my_fragment, container, false);
        mTxtFragment = view.findViewById(R.id.txt_home_fragment);
        mBtnFragment = view.findViewById(R.id.btn_go_detail);

        if(Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation) {
            mBtnFragment.setVisibility(View.GONE);
            mTxtFragment.setTextColor(Color.WHITE);
        }

        mBtnFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Configuration.ORIENTATION_PORTRAIT == getResources().getConfiguration().orientation) {
                    MyListFragment myListFragment = new MyListFragment();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container_layout, myListFragment).addToBackStack(null).commit();
                }
            }
        });

        return view;
    }

}
