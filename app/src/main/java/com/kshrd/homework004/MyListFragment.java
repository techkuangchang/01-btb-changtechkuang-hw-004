package com.kshrd.homework004;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MyListFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

    List<EmailModel> emailModelList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recycler_view, container, false);

        mRecyclerView = view.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

        emailModelList = new ArrayList<>();
        emailModelList.add(new EmailModel("soktry001@gmail.com"));
        emailModelList.add(new EmailModel("soktry002@gmail.com"));
        emailModelList.add(new EmailModel("soktry003@gmail.com"));
        emailModelList.add(new EmailModel("soktry004@gmail.com"));
        emailModelList.add(new EmailModel("soktry005@gmail.com"));
        emailModelList.add(new EmailModel("soktry006@gmail.com"));
        emailModelList.add(new EmailModel("soktry007@gmail.com"));
        emailModelList.add(new EmailModel("soktry009@gmail.com"));
        emailModelList.add(new EmailModel("soktry010@gmail.com"));
        emailModelList.add(new EmailModel("soktry011@gmail.com"));
        emailModelList.add(new EmailModel("soktry012@gmail.com"));
        emailModelList.add(new EmailModel("soktry013@gmail.com"));
        emailModelList.add(new EmailModel("soktry014@gmail.com"));
        emailModelList.add(new EmailModel("soktry015@gmail.com"));
        emailModelList.add(new EmailModel("soktry016@gmail.com"));
        emailModelList.add(new EmailModel("soktry017@gmail.com"));
        emailModelList.add(new EmailModel("soktry018@gmail.com"));
        emailModelList.add(new EmailModel("soktry019@gmail.com"));
        emailModelList.add(new EmailModel("soktry020@gmail.com"));
        emailModelList.add(new EmailModel("soktry021@gmail.com"));

        mAdapter = new MyCustomAdapter(getActivity(),emailModelList);

        mRecyclerView.setAdapter(mAdapter);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
}
